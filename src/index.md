---
home: false
title: Installation
heroImage: https://v1.vuepress.vuejs.org/hero.png
tagline: Fundraising and Donation WordPress plugin
actionText: Quick Start →
actionLink: /guide/
features:
- title: Feature 1 Title
  details: Feature 1 Description
- title: Feature 2 Title
  details: Feature 2 Description
- title: Feature 3 Title
  details: Feature 3 Description
footer: Made by Webinane with ❤️
---

**Note:** *In case of any problem regarding Lifeline Donation Pro, we recommend to join our forum at Support Forum or email us at <a href="mailto:support@webinane.com">support@webinane.com</a>*

Thanks for your patience and appreciation—the premium WordPress version of the Lifeline Donation Pro pluign is finally out! It comes with super-responsive layout, extreme flexibility, advanced coding and neat & clean design, which are the most demanded features for such a magnificent product devoted to the holy/virtuous cause.

With Lifeline Donation Pro premium WordPress plugin you can not only launch a perfect and progressing, Charity and NGO website,

Additional appeal is rendered to Lifeline Donation Pro through, cross browser compatibility.

## Our Motto
The sole determination of our highly skilled and devoted team workers is to bring innovation in the field of template development that will provide the users with something that they would have never experienced before. 

While the website designing is becoming a promising business, there are also many serious and complex issues that are being faced by the global web community. The same, otherwise disappointing, problems are being addressed here for the utmost facilitation and convenience of the clients.

## The Plugin Package Includes
- Currency Switcher 
- 2 Payment Gateways 
 1- Offline
 2- PayPal 
- Causes 
- Projects 
- 3 Popup Styles can be use as page 
- Predefined Donation Amount
- Custom Donation amount
- Recurring Donations 
- Custom Dropdown
- General Popup 
- Post type popup
- Menu Button 
- Donation History Short Code
- Email Settings
- Donor management System 
- Donation Report 
- Donation Analytics Dashboard 
- Compatible with all modern page builders 
- 5 Banner short Codes 
- 4 Causes Short Codes 
- 5 Donation Widgets 


##  Required plugins for Lifeline Donation Pro
- Lifeline Donation Pro 


