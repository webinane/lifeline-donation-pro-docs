---
home: false
---
**Installation Through Zip File**
- **Step 1:** Downlaod Plugin from Your https://codecanyon.net Dashboard
- **Step 2:** Uplaod the Pluign Zip folder on your website.
- **Step 3:** First go to WordPress Dashboard.
- **Step 4:** Go to Plugin "Add New" it will take you to pluing installation page. 
- **Step 5:** Locate and click on “Upload Plugin” button given in header menu. Go to “Choose File” and give path of zipped wordpress for upload it.
- **Step 6:** The uploaded zipped file will appear in your Wordpress Plugin Dashboard 
- **Step 7:** Once it will finish the uplaod. Click on activate plugin. 
- **Step 8:** Congratulation You have succefully installed plugin. 
- **Step 9:** Now Add Plugin Settings and start Receiving Donations. 




##  1: How to Configure Lifeline Donation Pro?


- **Follow the given steps:**

- **Step 1:** After activating plugin go to Lifeline Donation Pro General Setting page --> Address Info 

![Alt Text](./images/Address-setting-page.png)

- **1:** This is address setting page here you will add your Address. 
- **2:** Select your base Country and state.
- **3:** City & Office location such as office address with Zip Code. 
- **Not!** When a donor donates, he will receive a donation receipt with that address on.   



![Alt Text](./images/currency-info-page.png)

- **Step 2:** Select your site default curreny it will show up on your donation pages or popus.

- **1:** Select your site default currency. 
- **2:** Select currency position.
- **3:** Add thousend separator.
- **4:** Add deicimal separator. 
- **5:** Add number of deicimals in your currency.



![Alt Text](./images/payment-setting-page-1.png)

- **Step 3:** On this page you can test payment gateway or select a payment gateway for donation collection. 

If you want get a new payment gateway then click on No 5 for purchasing a new payment gateway. 

Also have option to selection Defualt payment method for your website. 


- **1:** Click on test mode button for payment method testing. 
- **2:** Activate payment gateway for your site by clicking on check mark or you can also check mark multiple payment gateways at once for donation collections..
- **3:** Select default payment gatewy from dropwon.
- **4:** If you need additional payment gateways **click Here** it will take you the extionsion page here you can get your country supported gateway. 

- **Step 4:** After configuring general settings, select your preferred payment gateway from the list.
![Alt Text](./images/payment-gateways-link.png)

- **Step 5:**  You can find out which payment gateways support your country by visiting our website https://www.webinane.com/plugins/

![Alt Text](./images/find-your-payment-gateway.png)
- After purchasing the payment gateway extension, integrate it into your website and configure it according to instructions.

![Alt Text](./images/payment-general-setting-page-2.png)

- **Step 6:** Add your payment gateway credentials.

**Note** each payment gateway requires different details. 

If you find any difficulty submit a support ticket or contact your payment gate proivders. 



![Alt Text](./images/general-settings-1.png)

- **Step 7:** This is donation general settings page. 
- Enable Plugin Style. 
Enabling it will apply the plugin styling to all donation pages and popups 

- By enabling the Causes and Projects , your website will be able to create custom post types and you can create as many causes and projects as you like. 

- On the Donation page or pop-ups, allow donors to choose multiple currencies. 

- With the pre-defined amounts and custom amounts enabled, these options will appear on donation pages and popups.

- Allow donors to make recurring contributions.

- On donation pages or pop-ups with Custom Dropdowns, donors can choose their favorite causes and projects.


![Alt Text](./images/general-settings-2.png)

- **Step 8:** Choose your favorite donation popup style and make the necessary changes on it. 
- Add the required general donation amount.
- You can add a background image.
- Add titles and subtitles for donations. 
- Add descriptions to donations. 
- Donors would be able to see how far the donation has progressed by enabling the donation progress bar in popups.


![Alt Text](./images/general-settings-3.png)

- **Step 9:** After setting up your donation popups and donation pages this is the last thing to adjust for showing the Donation button in your site header.

- Moreover, it can be disabled or customized according to the color scheme of your site and added a custom donation button name.




![Alt Text](./images/general-settings-4.png)

- **Step 10:** In addition, you can set up different Donation buttons for custom posts, such as projects and causes. 

- As Lifeline donation pro supports 3 methods of collecting donations, the user can click on the donation button to display pop-ups, or the pop-ups can be set up as a donation page, or they can be redirected to the external URL.

![Alt Text](./images/display-settings-page.png)

- **Step 11:**  On this tab, you can choose from three different pages to show donors after they have completed their donation. 
- Select a thank you page or donation success page.
- My account page.
- Donation history page

![Alt Text](./images/email-setting-page.png)

- **Step 12:** On this Email setting tab first configure email general settings. 

- In the email heading, include the site name or admin name of the sender.
- You will enter your outgoing email address here.
- Additionally, you can customize email templates with these options. 

![Alt Text](./images/admin-email-text-page.png)

- **Step 12:** In this tab, you can add the email text and heading for your automatic email sending from your website.


- **Step 12:** Congratulation you have successfully installed Dafolio Demo.


- **Step 12:** **Best Of Luck!**