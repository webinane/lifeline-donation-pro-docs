
# Dashboard

![Alt Text](./images/informative-dashboard.png)

## Performance dashboard is customizable with single click.
- Projects revenue
- Causes revenue
- General donation revenue
- Donation all the time
- Pending donations
- Cancelled donations

## Charts reporting for each donation type
- Projects gross revenue
- Charities gross revenue
- General gross revenue
- Other gross revenue
- Total revenue to date
- Recurring donation to date

## Donation Reports Dashbaord 

![Alt Text](./images/donation-reports.png)

- Dashboard for all collected donation reports.
- Have the option of adding manual donations as well.
- Export Donation reports

## Donors Dashboard 

![Alt Text](./images/donors-dashboard.png)

- You can view all donor lists here.
- You can search donors by name or email address in the search bar


