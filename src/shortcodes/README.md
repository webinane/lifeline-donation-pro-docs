
# Shortcodes

## Parallax Donation

Parallax donation shortcodes is used to display donation button either for showing donation popup or for custom link.
Example: `[button bg_image=36]`


#### Attributes

- **bg_image** : Provide the media ID to display as background image of the parallax section
- **parallax_layer_color** : Provde the hexa color code to overlay on Parallax
- **icon_type** : available options are `image` and `fontawesome`
- **campaign_icon** : Provide the css class for font awesome icon if `icon_type` is `fontawesome`
- **icon_image** : Provide the media id for icon if `icon_type` is `image`
- **title** : Title of the parallax
- **text** : Enter the text to show in Parallax
- **button** : True or Flase whether to display button 1
- **btn_label** : If button 1 is true then enter the label of that button
- **action** : Available options are `link_add` and `donate`. Choose donation if you want to display donation popup on button click
- **link** : If you selected the `link_add` in `action` then provide the link to set for the button
- **post_id** : If you selected the `donation` in `action` then provide the cause/project id to colelct the donation for.
- **popup_style** - If you selected the `donation` in `action` then provide the popup style. available options are `1`,`2`, `3`.
- **button1_bg_color** : Provide hexa color code for the button background
- **button1_bg_hover** : Provide hexa color code for the button hover background.
- **button1_color** : Provide hexa color code for the button text color.
- **button1_color_hover** : Provide hexa color code for the button text color hoveer.
- **button2** : True or Flase whether to display button 2
- **btn2_label** : If button 1 is true then enter the label of that button 2
- **action2** : Available options are `link_add` and `donate`. Choose donation if you want to display donation popup on button 2 click
- **link2** : If you selected the `link_add` in `action` then provide the link to set for the button 2
- **post_id2** : If you selected the `donation` in `action` then provide the cause/project id to colelct the donation for. It would be for button 2.
- **button2_color** : Provide hexa color code for the button text color.

## Campaign Style 1. 

Campaign Style 1 shortcode is used to display either the cause or project post according to the selected post type. Example `[wi_donation_campaigns1 post="project" num="2"]` 

#### Attributes. 

- **Post**: Choose post type for Campaign. Available options are `causes` and `projects```. 
- **num**: Provide the number for Campaigns to show. 
- **Cat1**: Provide the category id for cause if you select the cause from the `post` attribute.
- **Cat2**: Provide the category id projects if you select the projects from the `post` attribute.
- **order**: Provide the sort order to sort the Campaigns. Available options are ASC and DESC. 
- **button**: True or False whether to display button. 
- **Action**: Available options are `popup` and `detail`. Choose popup if you want to display donation popup on button click. 
- **button_label**: If `action` attribute option popup, then this attribute show as a button label. 
- **btn_label**: If `action` attribute option `detail`, then this attribute show as a button label. 

## Campaign Style 2. 

Campaign Style 2 shortcode is used to display either the cause or project post according to the selected post type. Example `[wi_donation_campaigns2 post="project" num="2"]` 

#### Attributes. 

- **Post**: Choose post type for Campaign. Available options are `causes` and `projects`. 
- **num**: Provide the number for Campaigns to show. 
- **Cat1**: Provide category id for cause if you select the `causes` from the post attribute.
- **Cat2**: Provide category id for projects if you select the `projects` from the post attribute.
- **order**: Choose order to sort the Campaigns. Available options are `ASC` and `DESC`. 
- **button**: `True` or `False` whether to display button. 
- **Action**: Available options are `popup` and `detail`. Choose popup if you want to display donation popup on button click. 
- **button_label**: If `action` attribute option `popup`, then this attribute show as a button label. 
- **btn_label**: If `action` attribute option detail, then this attribute show as a button label. 


## Campaign Style 3. 

Campaign Style 3 shortcode is used to display either the cause or project post according to the selected post type. Example `[wi_donation_campaigns3 post="project" num="2"]` 

#### Attributes. 

- **Post**: Choose post type for Campaign. Available options are `causes` and `projects`. 
- **num**: Provide the number for Campaigns to show. 
- **Cat1**: Provide category id for cause if you select the `causes` from the post attribute.
- **Cat2**: Provide category id for projects if you select the `projects` from the post attribute.
- **order**: Choose order to sort the Campaigns. Available options are `ASC` and `DESC`. 
- **button**: `True` or `False` whether to display button. 
- **Action**: Available options are `popup` and `detail`. Choose popup if you want to display donation popup on button click. 
- **button_label**: If `action` attribute option `popup`, then this attribute show as a button label. 
- **btn_label**: If `action` attribute option detail, then this attribute show as a button label.


## Campaign Style 4

Campaign Style 5 shortcode is used to display either the cause or project post according to the selected post type. Example `[wi_donation_campaigns4 post="project" num="2"]` 

#### Attributes. 

- **Post**: Choose post type for Campaign. Available options are `causes` and `projects`. 
- **num**: Provide the number for Campaigns to show. 
- **Cat1**: Provide category id for cause if you select the `causes` from the post attribute.
- **Cat2**: Provide category id for projects if you select the `projects` from the post attribute.
- **order**: Choose order to sort the Campaigns. Available options are `ASC` and `DESC`. 
- **button**: `True` or `False` whether to display button. 
- **Action**: Available options are `popup` and `detail`. Choose popup if you want to display donation popup on button click. 
- **button_label**: If `action` attribute option `popup`, then this attribute show as a button label. 
- **btn_label**: If `action` attribute option detail, then this attribute show as a button label.

## Campaign Style 5

Campaign Style 5 shortcode is used to display either the cause or project post according to the selected post type. Example `[wi_donation_campaigns post="project" num="2"]` 

#### Attributes. 

- **Post**: Choose post type for Campaign. Available options are `causes` and `projects`. 
- **num**: Provide the number for Campaigns to show. 
- **Cat1**: Provide category id for cause if you select the `causes` from the post attribute.
- **Cat2**: Provide category id for projects if you select the `projects` from the post attribute.
- **order**: Choose order to sort the Campaigns. Available options are `ASC` and `DESC`. 
- **button**: `True` or `False` whether to display button. 
- **Action**: Available options are `popup` and `detail`. Choose popup if you want to display donation popup on button click. 
- **button_label**: If `action` attribute option `popup`, then this attribute show as a button label. 
- **btn_label**: If `action` attribute option detail, then this attribute show as a button label.


Parallax Style 1\.

Parallax Style 1 shortcode is used with a parallax background image to display either showing donation popup or a custom link. Example 
    
    [wi_donation_parallax text="Enter text to show. "]


#### Attributes.

* **bg_image**: Provide the media ID to display as the background image of the parallax section.
* **title**: Enter the title to show in Parallax.
* **text**: Enter the text to show in Parallax.
* **button**: True or False whether to display button 1.
* **btn_label**: If button 1 is true then enter the label of that button
* **action**: Available options are `link_add` and `donation`. Choose donation if you want to display the donation popup on the button click.
* **link**: if you selected the `link_add` in `action` then provide the link to set for the button.
* **post_id**: If you selected the `donation` in `action` then provide the cause/project id to collect the donation for.
* **Button2**: `True` or `False` whether to display button 2.
* **btn2_label**: If button 2 is `true` then enter the label of that button 2.
* **action2**: Available options are `link_add` and `donation`. Choose donation if you want to display the donation popup on button 2 click.
* **link2**: If you selected the `link_add` in action then provide the link to set for the button 2.
* **post_id2**: If you selected the `donation` in action then provide the cause/project id to collect the donation for. It would be for button 2.



## Parallax Style 2.

Parallax Style 2 shortcode is used to display donation button with icon. Example  [wi_donation_parallax2 title="Title"]`

#### Attributes.

* **title**: Enter the title to show in Parallax.
* **sub_title**: Enter the subtitle to show in Parallax.
* **Button**: True or False whether to display button 1.
* **btn_label**: If button 1 is `true` then enter the label of that button
* **action**: Available options are `link_add` and donate. Choose donation if you want to display donation popup on button click.
* **link**: if you selected the `link_add` in action then provide the link to set for the button.
* **post_id**: If you selected the `donation` in action then provide the cause/project id to collect the donation for.



## Parallax Style 3  

Parallax Style 3 shortcode is used with parallax background image to display either for showing donation popup or a custom link. Example `[wi_donation_parallax3 title="Title"]`

#### **Attributes.** 

* **side_image: **Provide the media ID to display as side image of the parallax section. 
* **title: **Enter the title to show in Parallax. 
* **text**: Enter the text to show in Parallax. 
* **button**: True or False whether to display button 1. 
* **btn_label**: If button 1 is `true` then enter the label of that button 
* **action**: Available options are `link_add` and `donation`. Choose donation if you want to display donation popup on button click. 
* **link**: if you selected the `link_add` in `action` then provide the link to set for the button. 
* **post_id**: If you selected the `donation` in `action` then provide the cause/project id to collect the donation for. 
* **Button2**: True or Flase whether to display button 2. 
* **btn2_label**: If button 2 is `true` then enter the label of that button 2. 
* **action2**: Available options are `link_add` and `donation`. Choose donation if you want to display donation popup on button 2 click. 
* **link2**: If you selected the `link_add` in `action` then provide the link to set for the button 2. 
* **post_id2**: If you selected the `donation` in `action` then provide the cause/project id to collect the donation for. It would be for button 2. 

## Parallax Style 4

Parallax Style 4 shortcode is used with parallax background image to display donation popup. Example `[wi_donation_parallax4 title="Title"]`

#### **Attributes.** 

* **bg_image: **Provide the media ID to display as background image of the parallax section. 
* **title: **Enter the title to show in Parallax. 
* **text**: Enter the text to show in Parallax. 
* **button**: True or False whether to display button 1. 
* **btn_label**: If button 1 is `true` then enter the label of that button 
* **action**: Available options are `link_add` and `donation`. Choose donation if you want to display donation popup on button click. 
* **link**: if you selected the `link_add` in `action` then provide the link to set for the button. 
* **post_id**: If you selected the `donation` in `action` then provide the cause/project id to collect the donation for. 

## Parallax Style 5 

Parallax Style 5 shortcode is used with parallax background image to display donation popup. Example `[wi_donation_parallax5 title="Title"]`

#### **Attributes.** 

* **bg_image: **Provide the media ID to display as background image of the parallax section. 
* **title: **Enter the title to show in Parallax. 
* **text**: Enter the text to show in Parallax. 
* **Button**: True or False whether to display button 1. 
* **button_label**: If button 1 is `true` then enter the label of that button 
* **action**: Available options are `link_add` and `donate`. Choose donation if you want to display donation popup on button click. 
* **link**: if you selected the `link_add` in `action` then provide the link to set for the button. 
* **post_id**: If you selected the `donation` in `action` then provide the cause/project id to collect the donation for.

