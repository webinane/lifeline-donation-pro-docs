const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Lifeline Donation Pro',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],
  dest: 'public',
  base: '/lifeline-donation-pro-docs/',
  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    logo: '/logo.jpg',
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    nav: false,
    displayAllHeaders: true, // Default: false
    sidebar: {
      '/': [
        {
          title: 'Guide',
          collapsable: true,
          children:[
            {
              title: 'Intorduction',
              path: '/',
            },
            {
              title: 'Installation',
              path: '/installation/',
            },
            {
              title: 'Post Types',
              path: '/post-types/', 
            },
            {
              title: 'Settings', 
              path: '/settings/', 
            },
            {
              title: 'Shortcodes', 
              path: '/shortcodes/', 
            }
          ]
        }
      ],
    },
    plugins: ['@vuepress/active-header-links']
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
    '@vuepress/plugin-pwa',
    {
      serviceWorker: true,
      updatePopup: true
    }
  ]
}
