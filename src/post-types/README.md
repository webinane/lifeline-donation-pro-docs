
# Post Types
Common Settings:- Here you will see common post meta settings. All post types have same meta.
**Header Settings:**
- `Header Style` - You will see only one option here which is "Header style". If you want to set every page with different headers the you should this option.


**Title Settings:**
- `Show Title Section` - Enable to show title banner section on this page.
- `Header Banner Custom Title` - Enter the custom title for header banner section
- `Show Breadcrumb` - Click on check box if you want to show breadcrumb.
- `Title Section Background` - Upload background image for page title section.

**Sidebar Layout:**
- `Sidebar Layout` - You can se main content and sidebar alignment with this option.




## Causes
- - Go to"**Causes**" => "**Add New**".
- Add your title and explaination to the given places.

![Alt Text](./images/ns1.png)
- Add your causes custom details. 
- Add video code.
- Cause location name.
- Select your casue formate from the dropdown list. 
- Enter the donation amount needed. 

![Alt Text](./images/casuses-meta-page.png)

- Excerpts are optional hand-crafted summaries of your content that can be used in your theme.

![Alt Text](./images/ns9.png)

- You can also insert the **featured image** of your causes.

![Alt Text](./images/ns11.png)

## Projects
- Go to"Projects" => "Add New".
Add your title and explaination to the given places.

![Alt Text](./images/ns1.png)
- Excerpts are optional hand-crafted summaries of your content that can be used in your theme.

![Alt Text](./images/ns9.png)
- Add custom settings in Projects.
- Add Project needed amount
- Add Project location
![Alt Text](./images/project-meta-options.png)

- You can add **category** from the given section.

![Alt Text](./images/projects-22-2.png)
- You can also insert the **featured image** of your projects.

![Alt Text](./images/ns11.png)
